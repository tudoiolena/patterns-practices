import type { DraggableLocation } from "@hello-pangea/dnd";
import { Card, List } from "../common/types";

const reorderLists = (
  items: List[],
  startIndex: number,
  endIndex: number
): List[] => {
  const [removed] = items.splice(startIndex, 1);
  items.splice(endIndex, 0, removed);
  return items;
};

const reorderCards = (
  lists: List[],
  source: DraggableLocation,
  destination: DraggableLocation
): List[] => {
  const current: Card[] =
    lists.find((list) => list.id === source.droppableId)?.cards || [];
  const next: Card[] =
    lists.find((list) => list.id === destination.droppableId)?.cards || [];
  const target: Card = current[source.index];

  const isMovingInSameList = source.droppableId === destination.droppableId;

  if (isMovingInSameList) {
    const reordered: Card[] = reorderList(
      current,
      source.index,
      destination.index
    );
    return updateListsWithReorderedCard(lists, source.droppableId, reordered);
  }

  const newLists = lists.map((list) => {
    if (list.id === source.droppableId) {
      return {
        ...list,
        cards: removeCardFromList(current, source.index),
      };
    }

    if (list.id === destination.droppableId) {
      return {
        ...list,
        cards: addCardToList(next, destination.index, target),
      };
    }

    return list;
  });

  return newLists;
};

const reorderList = (
  cards: Card[],
  startIndex: number,
  endIndex: number
): Card[] => {
  const [removed] = cards.splice(startIndex, 1);
  cards.splice(endIndex, 0, removed);
  return cards;
};

const updateListsWithReorderedCard = (
  lists: List[],
  listId: string,
  reordered: Card[]
): List[] => {
  return lists.map((list) =>
    list.id === listId ? { ...list, cards: reordered } : list
  );
};

const removeCardFromList = (cards: Card[], index: number): Card[] => {
  return cards.slice(0, index).concat(cards.slice(index + 1));
};

const addCardToList = (cards: Card[], index: number, card: Card): Card[] => {
  return cards.slice(0, index).concat(card).concat(cards.slice(index));
};

export const reorderService = {
  reorderLists,
  reorderCards,
  removeCardFromList,
  addCardToList,
};
