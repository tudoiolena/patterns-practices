import { createServer } from "http";
import { Server, Socket } from "socket.io";

import { lists } from "./assets/mockData";
import { Database } from "./data/database";
import { CardHandler } from "./handlers/card.handler";
import { ListHandler } from "./handlers/list.handler";
import { ReorderService } from "./services/reorder.service";
import { FileLogger } from "./utils/logger/file-logger";
import { ConsoleLogger } from "./utils/logger/console-logger";
import { logger } from "./utils/logger/logger";
import { ReorderServiceProxy } from "./utils/reorder-service-proxy";

const PORT = 3003;

const httpServer = createServer();
const io = new Server(httpServer, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"],
  },
});

const db = Database.Instance;
const reorderService = new ReorderServiceProxy(logger);

if (process.env.NODE_ENV !== "production") {
  db.setData(lists);
}

const fileLogger = new FileLogger();
const consoleLogger = new ConsoleLogger();

logger.subscribe(fileLogger);
logger.subscribe(consoleLogger);

const onConnection = (socket: Socket): void => {
  new ListHandler(io, db, reorderService).handleConnection(socket);
  new CardHandler(io, db, reorderService).handleConnection(socket);
};

io.on("connection", onConnection);

httpServer.listen(PORT, () => console.log(`Listening on port: ${PORT}`));

export { httpServer };
