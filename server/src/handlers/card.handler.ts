import type { Server, Socket } from "socket.io";

import { CardEvent } from "../common/enums";
import { Card } from "../data/models/card";
import { SocketHandler } from "./socket.handler";
import { List } from "../data/models/list";
import { logger } from "../utils/logger/logger";
import { ReorderServiceProxy } from "../utils/reorder-service-proxy";
import { Database } from "../data/database";

export class CardHandler extends SocketHandler {
  protected reorderService: ReorderServiceProxy;

  constructor(io: Server, db: Database, reorderService: ReorderServiceProxy) {
    super(io, db, reorderService);
  }

  public handleConnection(socket: Socket): void {
    socket.on(CardEvent.CREATE, this.createCard.bind(this));
    socket.on(CardEvent.REORDER, this.reorderCards.bind(this));
    socket.on(CardEvent.DELETE, this.deleteCard.bind(this));
    socket.on(CardEvent.RENAME, this.renameCard.bind(this));
    socket.on(
      CardEvent.CHANGE_DESCRIPTION,
      this.changeCardDescription.bind(this)
    );
    socket.on(CardEvent.CLONE, this.cloneCard.bind(this));
  }

  private updateListsAndNotify(lists: List[]): void {
    this.db.setData(lists);
    this.updateLists();
  }

  public createCard(listId: string, cardName: string): void {
    try {
      const newCard = new Card(cardName, "");
      const lists = this.db.getData();

      const updatedLists = lists.map((list) =>
        list.id === listId ? list.setCards(list.cards.concat(newCard)) : list
      );

      this.updateListsAndNotify(updatedLists);

      const logMessage = `Card ${cardName} created in list id ${listId}`;
      logger.log(logMessage);
    } catch (error) {
      const errorMessage = `Error creating card in list id ${listId}: ${error.message}`;
      logger.error(errorMessage);
    }
  }

  public cloneCard(id: string): void {
    const lists = this.db.getData();
    let clonedCard: Card;
    let existingCard: Card;
    try {
      const updatedLists = lists.map((list) => {
        existingCard = list.cards.find((card) => card.id === id);
        console.log("existingCard", existingCard);
        if (existingCard) {
          clonedCard = existingCard.clone();
          const updatedCards = [...list.cards, clonedCard];

          return {
            ...list,
            cards: updatedCards,
            setCards: list.setCards,
          };
        }

        return list;
      });

      this.updateListsAndNotify(updatedLists);

      const logMessage = `Card "${clonedCard.name}" cloned successfully. ID of original card: ${id}, createdAt of original card: ${existingCard.createdAt}, Id of cloned card:: ${clonedCard.id}, createdAt of cloned card: ${clonedCard.createdAt}`;
      logger.log(logMessage);
    } catch (error) {
      const errorMessage = `Error cloning card: ${error.message}`;
      logger.error(errorMessage);
    }
  }

  public deleteCard(id: string): void {
    try {
      const lists = this.db.getData();
      const updatedLists = lists.map((list) => {
        const updatedCards = list.cards.filter((card) => card.id !== id);
        return {
          ...list,
          cards: updatedCards,
          setCards: list.setCards,
        };
      });
      this.updateListsAndNotify(updatedLists);
      const logMessage = `Card id:${id} deleted successfully`;
      logger.log(logMessage);
    } catch (error) {
      const errorMessage = `Error deleting a card, id: ${id} : ${error.message}`;
      logger.error(errorMessage);
    }
  }

  private renameCard(id: string, updatedTitle: string): void {
    try {
      const lists = this.db.getData();
      const updatedLists = lists.map((list) => ({
        ...list,
        setCards: list.setCards,
        cards: list.cards.map((card) => {
          if (card.id === id) {
            card.name = updatedTitle;
          }
          return card;
        }),
      }));
      this.updateListsAndNotify(updatedLists);
      const logMessage = `Card id: ${id} renamed successfully with new title: ${updatedTitle}`;
      logger.log(logMessage);
    } catch (error) {
      const errorMessage = `Error renaming card, id: ${id} : ${error.message}`;
      logger.error(errorMessage);
    }
  }

  private changeCardDescription(id: string, updatedDescription: string): void {
    try {
      const lists = this.db.getData();
      const updatedLists = lists.map((list) => ({
        ...list,
        setCards: list.setCards,
        cards: list.cards.map((card) => {
          if (card.id === id) {
            card.description = updatedDescription;
          }
          return card;
        }),
      }));
      this.updateListsAndNotify(updatedLists);
      const logMessage = `Card id: ${id} description successfully changed: ${updatedDescription}`;
      logger.log(logMessage);
    } catch (error) {
      const errorMessage = `Error updating card description, id: ${id} : ${error.message}`;
      logger.error(errorMessage);
    }
  }

  private reorderCards({
    sourceIndex,
    destinationIndex,
    sourceListId,
    destinationListId,
  }: {
    sourceIndex: number;
    destinationIndex: number;
    sourceListId: string;
    destinationListId: string;
  }): void {
    const lists = this.db.getData();
    const reordered = this.reorderService.reorderCards({
      lists,
      sourceIndex,
      destinationIndex,
      sourceListId,
      destinationListId,
    });
    this.updateListsAndNotify(reordered);
  }
}
