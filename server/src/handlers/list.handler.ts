import type { Server, Socket } from "socket.io";

import { ListEvent } from "../common/enums";
import { List } from "../data/models/list";
import { SocketHandler } from "./socket.handler";
import { logger } from "../utils/logger/logger";
import { ReorderServiceProxy } from "../utils/reorder-service-proxy";
import { Database } from "../data/database";

export class ListHandler extends SocketHandler {
  protected reorderService: ReorderServiceProxy;

  constructor(io: Server, db: Database, reorderService: ReorderServiceProxy) {
    super(io, db, reorderService);
  }

  public handleConnection(socket: Socket): void {
    socket.on(ListEvent.CREATE, this.createList.bind(this));
    socket.on(ListEvent.GET, this.getLists.bind(this));
    socket.on(ListEvent.REORDER, this.reorderLists.bind(this));
    socket.on(ListEvent.DELETE, this.deleteList.bind(this));
    socket.on(ListEvent.RENAME, this.renameList.bind(this));
  }

  private getLists(callback: (cards: List[]) => void): void {
    callback(this.db.getData());
  }

  private updateListsAndNotify(lists: List[]): void {
    this.db.setData(lists);
    this.updateLists();
  }

  private reorderLists(sourceIndex: number, destinationIndex: number): void {
    const lists = this.db.getData();
    const reorderedLists = this.reorderService.reorder(
      lists,
      sourceIndex,
      destinationIndex
    );

    this.updateListsAndNotify(reorderedLists);
  }

  private createList(name: string): void {
    const lists = this.db.getData();
    const newList = new List(name);
    try {
      this.updateListsAndNotify(lists.concat(newList));

      const logMessage = `List ${newList.name}, id:${newList.id} created successfully`;
      logger.log(logMessage);
    } catch (error) {
      const errorMessage = `Error creating list ${newList.name}, list id: ${newList.id} : ${error.message}`;
      logger.error(errorMessage);
    }
  }

  private deleteList(id: string): void {
    try {
      const lists = this.db.getData();
      const updatedLists = lists.filter((list) => list.id != id);

      this.updateListsAndNotify(updatedLists);
      const logMessage = `List id:${id} deleted successfully`;
      logger.log(logMessage);
    } catch (error) {
      const errorMessage = `Error deleting list, id: ${id} : ${error.message}`;
      logger.error(errorMessage);
    }
  }

  private renameList(id: string, updatedTitle: string): void {
    try {
      const lists = this.db.getData();
      const updatedLists = lists.map((list) =>
        list.id === id
          ? {
              ...list,
              name: updatedTitle,
              setCards: list.setCards,
            }
          : list
      );

      this.updateListsAndNotify(updatedLists);
      const logMessage = `List id: ${id} renamed successfully with new title: ${updatedTitle}`;
      logger.log(logMessage);
    } catch (error) {
      const errorMessage = `Error renaming list, id: ${id} : ${error.message}`;
      logger.error(errorMessage);
    }
  }
}
