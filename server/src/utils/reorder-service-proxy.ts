import { List } from "../data/models/list";
import { ReorderService } from "../services/reorder.service";
import { Logger } from "./logger/logger";

//PATTERN:{Proxy}
class ReorderServiceProxy extends ReorderService {
  private logger: Logger;

  constructor(logger: Logger) {
    super();
    this.logger = logger;
  }

  private logMethodCall(methodName: string, params: any): void {
    const logMessage = `Calling method '${methodName}' with parameters: ${JSON.stringify(
      params
    )}`;
    this.logger.log(logMessage);
  }

  reorder<T>(items: T[], startIndex: number, endIndex: number): T[] {
    this.logMethodCall("reorder", { items, startIndex, endIndex });
    return super.reorder(items, startIndex, endIndex);
  }

  reorderCards({
    lists,
    sourceIndex,
    destinationIndex,
    sourceListId,
    destinationListId,
  }: {
    lists: List[];
    sourceIndex: number;
    destinationIndex: number;
    sourceListId: string;
    destinationListId: string;
  }): List[] {
    this.logMethodCall("reorderCards", {
      lists,
      sourceIndex,
      destinationIndex,
      sourceListId,
      destinationListId,
    });
    return super.reorderCards({
      lists,
      sourceIndex,
      destinationIndex,
      sourceListId,
      destinationListId,
    });
  }
}

export { ReorderServiceProxy };
