import { Observer } from "./logger";

class ConsoleLogger implements Observer {
  public update(): void {}
}

export { ConsoleLogger };
