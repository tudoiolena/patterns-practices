import { createLogger, format, transports } from "winston";

const logger = createLogger({
  format: format.combine(format.timestamp(), format.json()),
  transports: [
    new transports.Console({ level: "error" }),
    new transports.File({ filename: "logfile.log" }),
  ],
});

//PATTERN:{Observer}
interface Observer {
  update(log: string): void;
}

class Logger {
  private observers: Observer[] = [];

  public subscribe(observer: Observer): void {
    this.observers.push(observer);
  }

  public unsubscribe(observer: Observer): void {
    this.observers = this.observers.filter((o) => o !== observer);
  }

  public notify(log: string): void {
    this.observers.forEach((observer) => observer.update(log));
  }

  public log(log: string): void {
    logger.info(log);
    this.notify(log);
  }

  public error(log: string): void {
    logger.error(log);
    this.notify(log);
  }
}

const globalLogger = new Logger();

export { globalLogger as logger, Observer, Logger };
